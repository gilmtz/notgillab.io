---
title: Week 5
date: 2019-02-24
---
![Gilberto Martinez](https://gitlab.com/NotGil/notgil.gitlab.io/raw/master/profile.jpg "Me")




**What did you do this past week?**


I did my class presentation for Professional Communications class.  With the right amount of practice, I performed better than I ever expected. I'm excited to continue practicing public speaking. After my class presentation, I picked up my car from the shop, and it felt free again. No longer dependent on someone else's schedule.

**What's in your way?**


I used to avoid any classes or events that might include writing or public speaking. So I have a lot of catching up to do but, I'm up for the challenge. Other than that I have to fight with my sleeping schedule. Last week I spent most nights playing Super Smash Bros Ultimate till 4 am. While great times I haven't been getting enough sleep and I've had to rely on coffee most days. 

**What will you do next week?**


Next week I will learn how to set up a Django back end for our RESTful API. I figure it's an excellent opportunity to learn a new package. I will also work on consuming less caffeine during the day for both health and budget reasons. On a more general note, I will begin to use a todo list app to help me organize. I have noticed I make promises to people and to myself to do something before the end of the day and I forget about it. 


**What did you think of the talk by Ed on GCP and Hannah on AWS?**


I overslept on the GCP talk so I can't speak to that. I did make the AWS talk. I used AWS with educational credits a few years ago, and it was confusing.  After this talk and some of Amazon's UI updates I'm more confident I can figure everything out. It also helps there's a budgeting tool, so I don't have to worry about a huge bill at the end of the month.

**What's your pick-of-the-week or tip-of-the-week?**


My tip of the week is, take courses that aren't your strong suit. University is a great sandbox to test things out. We can take many risks, and there are barely any consequences. It would be tough for me to practice public speaking in the real world without possibly messing up an important meeting or presentation. 





